#!/usr/bin/python

# Include the Dropbox SDK libraries
import dropbox

import os
import sys

DEBUG = False

TOKEN = ''
if os.path.exists('token.txt'):
	with open('token.txt') as token_f:
		TOKEN = token_f.readline().rstrip()

if DEBUG:
	print TOKEN

# Create client
client  = dropbox.client.DropboxClient(TOKEN)

if DEBUG:
	print "linked account:", client.account_info()

noParams = len(sys.argv)
if noParams < 2:
	print "Usage: upload_to_dropbox.py <file1> ..."
	sys.exit(0)

for idx in range(1, noParams):
	f = open(sys.argv[idx])
	filename = os.path.basename(sys.argv[idx])
	response = client.put_file(filename, f, True)
	print "uploaded:", response
	
#f = open('working-draft.txt')
#response = client.put_file('/magnum-opus.txt', f, True)
#print "uploaded:", response

#folder_metadata = client.metadata('/')
#print "metadata:", folder_metadata

#f, metadata = client.get_file_and_metadata('magnum-opus.txt')
#out = open('magnum-opus.txt', 'w')
#out.write(f.read())
#print(metadata)
